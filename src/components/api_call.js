import React from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import rrulePlugin from "@fullcalendar/rrule";
import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick
import seedrandom from 'seedrandom';


import "../App.css";

export default class EventManger extends React.Component {
  constructor(props) {
    super(props);
    const DaysEnum = [
      "LUNDI",
      "MARDI",
      "MERCREDI",
      "JEUDI",
      "VENDREDI",
      "SAMEDI",
      "DIMANCHE",
    ];
		let weekeven = [],weekodd = [];
		for (let i = 1;i<53;i++){
			if (i%2===0)
				weekeven.push(i);
			else
				weekodd.push(i);
		}

    const getDayOfWeekRank = (day) => DaysEnum.indexOf(day) + 1;
    function getRandomColor(name) {
			const rand = seedrandom(name);
      var letters = "0123456789ABCDEF";
      var color = "#";
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(rand() * 16)];
      }
      return color;
    }
    let calendarEvents = this.props.UVs.map((e) => {
			if (e.type === "Cours" || e.type === "TD") {
      return {
        backgroundColor: getRandomColor(e.uv),
        borderColor: getRandomColor(e.uv),
        title: e.uv,
        daysOfWeek: [getDayOfWeekRank(e.day.toString())],
        startRecur: new Date(2020, 1, 24),
        endRecur: new Date(2020, 5, 18),
        startTime: e.begin,
        endTime: e.end,
        allDay: false,
      };
			}
			else if (e.group.includes("semaine B")) {
			return {
        backgroundColor: getRandomColor(e.uv),
        borderColor: getRandomColor(e.uv),
        title: e.uv,
        //daysOfWeek: [getDayOfWeekRank(e.day.toString())],
        rrule: {
					freq: "weekly",
					dtstart: new Date(2020, 1, 24),
					until: new Date(2020, 5, 18),
					byhour: parseInt(e.begin.split(":")[0]),
					byminute: parseInt(e.begin.split(":")[1]),
					byweekday: getDayOfWeekRank(e.day),
        	byweekno: weekeven
     		},
				duration: "02:00",
        allDay: false,
      };
			}
			return {
        backgroundColor: getRandomColor(e.uv),
        borderColor: getRandomColor(e.uv),
        title: e.uv,
				rrule: {
          freq: "weekly",
          dtstart: new Date(2020, 1, 24),
          until: new Date(2020, 5, 18),
          byhour: parseInt(e.begin.split(":")[0]),
          byminute: parseInt(e.begin.split(":")[1]),
          byweekday: getDayOfWeekRank(e.day),
          byweekno: weekodd
        },
				duration: "02:00",
        allDay: false,
      };
    });
    this.state = {
      Loaded: false,
      currentRefinedEventMap: {},
      calendarWeekends: true,
      calendarEvents,
    };
  }

  render() {
    return (
      <div className="demo-app">
        <div className="demo-app-calendar">
          <FullCalendar
            defaultView="dayGridMonth"
            header={{
              left: "prev,next today",
              center: "title",
              right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek",
            }}
            plugins={[
              dayGridPlugin,
              timeGridPlugin,
              interactionPlugin,
              rrulePlugin,
            ]}
            weekends={this.state.calendarWeekends}
            events={this.state.calendarEvents}
            />
        </div>
      </div>
    );
  }

  toggleWeekends = () => {
    this.setState({
      calendarWeekends: !this.state.calendarWeekends,
    });
  };
}

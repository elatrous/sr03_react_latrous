import React from "react";
import logo from "../logo.svg";
const Body = (props) => (
  <div>
    <img src={logo} class="App-logo" alt="logo"></img>
    <p> to get started say please {props.text} </p>
  </div>
);

export default Body;

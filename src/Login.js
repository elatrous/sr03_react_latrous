import React from 'react';
import {useHistory} from 'react-router-dom';

function Login() {
	let history = useHistory();
	function setCookie() {
  	let val = document.getElementById("login").value;
  	document.cookie = `login=${val}`;
		history.push("/timetable");
	}
	return (
		<div style={{textAlign: "center"}}>
			<label>nom</label>
			<input type="text" id="login" />
			<input value="Login" type="submit" onClick={setCookie} style={{marginBottom:"75vh"}}/>
		</div>
	);
}

export default Login;

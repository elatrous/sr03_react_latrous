import React from "react";
import EventManger from "./components/api_call";
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      uvs: [],
    };
  }

  componentDidMount() {
		//jbarthel
		let name = document.cookie.split("=")[1];
    fetch(
      `https://cors-anywhere.herokuapp.com/https://webapplis.utc.fr/Edt_ent_rest/myedt/result/?login=${name}`
    )
      .then((res) => res.json())
      .then(
        (result) => {
          //console.log(result);
          this.setState({
            isLoaded: true,
            uvs: result,
          });
        },
        // Remarque : il est important de traiter les erreurs ici
        // au lieu d'utiliser un bloc catch(), pour ne pas passer à la trappe
        // des exceptions provenant de réels bugs du composant.
        (error) => {
          if (error) {
            console.log(error);
          }
        }
      );
  }

  render() {
    const { error, isLoaded, uvs } = this.state;
    if (error) {
      return <div>Erreur : {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Chargement…</div>;
    } else {
      return (
          <EventManger UVs={this.state.uvs} />
      );
    }
  }
}

export default App;
